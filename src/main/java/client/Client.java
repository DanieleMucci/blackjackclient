package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.UnknownHostException;
import java.security.Security;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.swing.JOptionPane;

import com.sun.net.ssl.internal.ssl.Provider;

/**
 * 
 * Implement the client of the game. This class has just to connect to the
 * server and display output/collect and send the input for the player.
 * First, the init() method is called. It takes care of initiating network objects and connecting through SSL to the server. In order to do so, the certificate of the server 
 * must be added to the trusted certificates. To do so for a manually-created certificate , it is possible to run the program InstallCert with argument localhost:4443  
 * before running this client.
 * @author Daniele Mucci
 *
 */
public class Client {
	private static SSLSocket sslSocket;
	private static PrintWriter out;
	private static BufferedReader in;
	private static int money;
	private static int bet;

	/**
	 * Init function for socket , printwriter and BufferedReader. It also ask from the user the amount of money he/she wants to play with.
	 * 
	 * @throws IOException
	 * @throws UnknownHostException
	 */
	public static void init() throws UnknownHostException, IOException {
		Security.addProvider(new Provider());
		SSLSocketFactory sslsocketfactory = (SSLSocketFactory)SSLSocketFactory.getDefault();
		String fullAddress=JOptionPane.showInputDialog("Insert the address of the server (localhost)");
		int port=0;
		try {
			port=Integer.parseInt(JOptionPane.showInputDialog("Insert the port to connect to (4443)"));
		}
		catch (Exception e) {
			System.out.println("The port was not valid. Shutting down");
			closeGame();
		}
		sslSocket = (SSLSocket)sslsocketfactory.createSocket(fullAddress,port);
		out = new PrintWriter(sslSocket.getOutputStream(), true);
		in = new BufferedReader(new InputStreamReader(
				sslSocket.getInputStream()));
		System.out.println("Welcome to Blackjack game!");
		try {
			money = Integer.parseInt(JOptionPane.showInputDialog("Please insert the amount of money you want to play with"));
		} catch (Exception e) {
			System.out
					.println("The amount inserted is not valid. Closing game");
			closeGame();
		}
	}

	/**
	 * Main function of the client. First calls the init() method , then it just reads all messages from the server and passes it to the parsing method to react and respond.
	 * @param args
	 * @throws UnknownHostException
	 * @throws IOException
	 */
	public static void main(String[] args) throws UnknownHostException,
			IOException {
		init();
		String line = in.readLine();
		boolean error;
		while (line != null) {
			error = parseCmd(line);
			if (error == false) {
				// Error in the message
				System.out.println("Error Detected. Closing game!");
				closeGame();
			}
			line = in.readLine();
		}

	}

	/**
	 * Function used to distinguish between REQT , DATA and REST
	 * 
	 * @param command
	 *            is the message sent by the server
	 * @return true if no errors were detected, false if an error has been
	 *         encountered
	 */
	public static boolean parseCmd(String command) {
		String header = command.substring(0, 4);
		String cmd = command.substring(4);
		if (header.equals("REQT"))
			return parseREQT(cmd);
		else if (header.equals("DATA"))
			return parseDATA(cmd);
		else if (header.equals("REST"))
			return parseREST(cmd);
		else
			return false;
	}

	/**
	 * Method which takes care of parsing requests
	 * 
	 * @param command
	 *            is the command part of the message from the server (i.e.
	 *            bytes(4-end) ) consisting of the request
	 * @return true if no errors, false if an error has been encountered
	 */
	public static boolean parseREQT(String command) {
		if (command.equals("NAME")) {
			String name = JOptionPane
					.showInputDialog("Please insert your name");
			if (name.length() > 0) {
				out.println("DATA" + name);
				return true;
			} else {
				System.out.println("Sorry, name is not valid!");
				return false;
			}

		} else if (command.equals("BET")) {
			String betString = JOptionPane
					.showInputDialog("Please insert your bet in range 0 - "
							+ money);
			int betInt = Integer.parseInt(betString);
			while (betInt > money || betInt < 1) {
				System.out
						.println("Sorry, the amount of this bet is not valid. Insert a new one");
				betString = JOptionPane
						.showInputDialog("Please insert your bet in range 0 - "
								+ money);
				betInt = Integer.parseInt(betString);
			}
			bet = betInt;
			out.println("DATA" + betString);
			return true;

		} else if (command.equals("COMMAND")) {
			String decision = JOptionPane
					.showInputDialog(
							"Please insert your decision.S for stand , H for HIT and D for double")
					.substring(0, 1);
			while (!(decision.equals("H") || decision.equals("S") || decision
					.equals("D"))
					|| (decision.equals("D") && (money - 2 * bet < 0))) { 
				System.out
						.println("Sorry, your decision is invalid. Please insert it again");
				JOptionPane
						.showMessageDialog(null,
								"Sorry, your decision is invalid. Please insert it again");
				decision = JOptionPane
						.showInputDialog("Please insert your decision.S for stand , H for HIT and D for double");
			}
			out.println("DATA" + decision);
			return true;
		} else if (command.equals("COMMANDNODOUBLE")) {
			String decision = JOptionPane.showInputDialog(
					"Please insert your decision.S for stand or H for HIT ")
					.substring(0, 1);
			System.out.println(decision);
			while (!(decision.equals("H") || decision.equals("S"))) {
				System.out
						.println("Sorry, your decision is invalid. Please insert it again");
				JOptionPane
						.showMessageDialog(null,
								"Sorry, your decision is invalid. Please insert it again");
				decision = JOptionPane
						.showInputDialog("Please insert your decision.S for stand or H for HIT");
			}
			out.println("DATA" + decision);
			return true;
		} else if (command.equals("NEW_GAME")) {
			int new_game=JOptionPane.showConfirmDialog(null, "Would you like to play a new game?","NEW GAME",JOptionPane.YES_NO_OPTION);
			if (new_game==JOptionPane.YES_OPTION) {
				out.println("DATAYES");
				bet = 0;
				System.out.println("Your current amount of money is:" + money);
				return true;
			} else{
				out.println("DATANO");
				closeGame();
				
			}
			return true;
		} else
			return false;
	}

	/**
	 * Method which takes care of parsing data messages from server. This method
	 * basically just displays the cards string sent from server.
	 * 
	 * @param command
	 *            is the card string from the server
	 * @return
	 */
	public static boolean parseDATA(String command) {
		System.out.println("The cards currently on the table are:");
		System.out.println(command);
		return true;
	}

	/**
	 * Method which takes care of displaying the result to the player.
	 * 
	 * @param command
	 *            is the result sent from the server (i.e. WIN or LOSE)
	 * @return
	 * @throws IOException
	 */
	public static boolean parseREST(String command) {
		if (command.equals("WIN")) {
			System.out.println("You won the game!");
			System.out.println("You just gained " + bet + " money!");
			money = money + bet;
		} else if (command.equals("BJ")) {
			System.out.println("BLACKJACK!");
			System.out.println("You just gained " + (int)(1.5*bet) + " money!");

			money = (int)(money + 1.5 * bet);
		} else {

			System.out.println("You lost the game!");
			money = money - bet;
			if (money == 0) {
				System.out
						.println("You finished your money. Good luck next time!");
				closeGame();

			}
		}
		return true;
	}

	/**
	 * Method which takes care of the closing of game.
	 * 
	 * @throws IOException
	 */
	private static void closeGame() {
		try {
			sslSocket.close();
			in.close();
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.exit(0);
	}

}
