This is the simple client implementation of blackjack application.

The connection between server and client uses SSL; the certificate of the server has been generate with keytool and therefore it is generally NOT TRUSTED.

In order to avoid SSL errors during handshake it is necessary to launch this client setting the trustStore property and include the certificate of the server as trusted.

To do so, a file /src/main/resources/jssecacerts has been created. This represents a trustStore which includes the server certificate.

To launch the client it is necessary to :

$mvn install
$cd target
$java -Djavax.net.ssl.trustStore=classes/jssecacerts -jar DanieleMucciBlackjackClient-1.0.jar

This will set as trustStore the store providen with the client and will allow secure communication between client and server.


